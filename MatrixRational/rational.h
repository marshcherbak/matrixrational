#pragma once
#include <iostream>

class Rational
{
public:
	Rational();
	Rational(double val);
	Rational& operator =(const Rational & other);
	bool operator ==(Rational other);
//	bool operator ==(double val);
	Rational operator +(const Rational & other);
	Rational operator =(double el);
	Rational operator -(const Rational & other);
	Rational operator *(const Rational & other);
	Rational operator /(const Rational & other);
	operator double ();
	int gcd(int a, int b);
	Rational& simplify();
	Rational approximate();
	Rational pow(const Rational& r, double p);

	friend std::ostream& operator<<(std::ostream& os, Rational& r);
	friend std::istream& operator>>(std::istream& is, Rational& r);
private:
	long m;
	long n;
};

