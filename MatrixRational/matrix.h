﻿#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>
#include <vector>

using namespace std;
template <class T> class Matrix {
public:
	Matrix();
	Matrix( vector< vector<T> > val);
	Matrix<T> operator =(const Matrix &other);
	bool operator==(const Matrix&other);
	Matrix<T> operator +(Matrix &other);
	Matrix<T> operator -(Matrix &other);
	Matrix<T> operator *(Matrix &other);
	Matrix<T> operator *(T num);
	Matrix<T> operator ~();
	Matrix <T> Transpose();
	Matrix<T> findMinor(int r, int c);
	Matrix<T> Gauss(Matrix&right);
	vector<T> getColumn(int i);
	vector<T> ek(int len, int k);
	T norm(vector<T>x);
	vector<T> alphae(T sqrt, vector<T> e);
	vector <T> findU(vector<T>x, int k);
	vector<T> findV(vector<T> u);
	Matrix<T> makeQRI(int size);
	Matrix<T> findQi();
	Matrix<T> makeBigger(int num);
	Matrix<T> QRDec(Matrix&A);

	T findGreatestND(int &i, int &j);
	Matrix<T> makeGivens(int x, int y, T a, int n);
	vector<T> Jacobi();

	friend ostream& operator<<(ostream& os, Matrix<T>& matr) {
		int n = matr.elements.size();
		int m = matr.elements[0].size();
		for (int i = 0; i < n; ++i) {
			for (int j = 0; j < m; ++j)
				 os << matr.elements[i][j] << " ";
			os << "\n";		
		}
		return os;
		
	}
	friend istream& operator>>(istream & is, Matrix<T> & matr) {
		cout << "Number of rows: ";
		int r;
		int c;
		is >> r;
		cout << "Number of columns: ";
		is >> c;
		for (int i = 0; i < r; ++i) {
			cout << "Row " << i << ": ";
			vector<T> temp;
			for (int i = 0; i < c; ++i) {
				T el;
				is >> el;
				temp.push_back(el);
				
			}
			matr.elements.push_back(temp);
			cout << "\n";
			
		}
		return is;
	}
private:
	vector< vector<T> > elements;
};



template <class T> Matrix<T>::Matrix() {}

template<class T>
Matrix<T>::Matrix(vector< vector<T> > val)
{
	elements.swap(val);
}

template<class T>
Matrix<T> Matrix<T>::operator=(const Matrix<T> & other)
{
	vector<vector<T>> temp = other.elements;
	elements.swap(temp);
	return *this;
}

template<class T>
inline bool Matrix<T>::operator==(const Matrix & other)
{
	if (elements.size() != other.elements.size() || elements[0].size() != other.elements[0].size())
		return false;
	else {
		int n = elements.size();
		int m = elements[0].size();
		for (int i = 0; i < n; ++i)
			for (int j = 0; j < m; ++j)
				if (!(elements[i][j] == other.elements[i][j]))
					return false;
		return true;
	}
	return false;
}

template<class T>
Matrix<T> Matrix<T>::operator+(Matrix<T> & other)
{
	if ((elements.size() != other.elements.size()) || (elements[0].size() != other.elements[0].size()))
		throw exception("\nCan't add matrix with different dimentions.\n");
	else {
		int r = elements.size();
		int c = elements[0].size();
		Matrix<T> res;

		for (int i = 0; i < r; ++i) {
			vector<T> temp;
			for (int j = 0; j < c; j++)
				temp.push_back(elements[i][j] + other.elements[i][j]);
			res.elements.push_back(temp);
		}
		return res;
	}
}

template<class T>
Matrix<T> Matrix<T>::operator-(Matrix<T> & other)
{
	if ((elements.size() != other.elements.size()) || (elements[0].size() != other.elements[0].size()))
		throw exception("\nCan't add matrix with different dimentions.\n");
	else {
		int r = elements.size();
		int c = elements[0].size();
		Matrix<T> res;

		for (int i = 0; i < r; ++i) {
			vector<T> temp;
			for (int j = 0; j < c; j++)
				temp.push_back(elements[i][j] - other.elements[i][j]);
			res.elements.push_back(temp);
		}
		return res;
	}
}

template<class T>
Matrix<T> Matrix<T>::operator*(Matrix<T> & other)
{
	if (elements[0].size() != other.elements.size())
		throw exception("\nCan't multiply.\n");
	else{
		int r1 = elements.size(), r2 = other.elements.size();
		int c1 = elements[0].size(), c2 = other.elements[0].size();
		Matrix<T> res;
		for (int i = 0; i < r1; ++i) {
			vector<T> temp;
			for (int j = 0; j < c2; ++j) {
				T s = 0;
				for (int k = 0; k < c1; k++) {
					s = s + elements[i][k] * other.elements[k][j];
				}
				temp.push_back(s);
			}
			res.elements.push_back(temp);
		}
		return res;
	}
	
}

template<class T>
inline Matrix<T> Matrix<T>::operator*(T num)
{
	int m = elements.size();
	int n = elements[0].size();
	for (int i = 0; i < m; ++i)
		for (int j = 0; j < n; ++j)
			elements[i][j] = elements[i][j] * num;
	return *this;
}

template<class T>
Matrix<T> Matrix<T>::operator~()
{
	int rows = elements.size();
	int columns = elements[0].size();
	int count;
	vector<vector<T>> s;
	for (int i = 0; i < rows; ++i) {
		vector<T> temp;
		for (int j = 0; j < columns; ++j) {
			if (i == j)
				temp.push_back(1);
			else
				temp.push_back(0);
		}
		s.push_back(temp);
	}
	Matrix<T> ones(s);

	for (int i = 0; i < rows; ++i) {
		T a = elements[i][i];

		for (int m = 0; m < columns; m++) {
			elements[i][m] = elements[i][m]/a;
			ones.elements[i][m] = ones.elements[i][m]/a;

		}

		for (int n = 0; n < rows; ++n) {
			if (n != i) {
				count = 0;
				T b = elements[n][i];
				for (int j = 0; j < columns; ++j) {
					elements[n][j] = elements[n][j]-(elements[i][j] * b);
					T tem = ones.elements[i][j] * b;
					tem = ones.elements[n][j] - tem;
					ones.elements[n][j] = tem;
					T z = 0;
					if (elements[n][j] == z)
						count++;
				}
				if (count == columns)
					throw exception("Determinant is 0. Can't solve.\n");
			}
		}
	}
	return ones;
}

template<class T>
Matrix<T> Matrix<T>::Transpose()
{
	vector<vector <T>> init;
	int m = elements[0].size();
	int n = elements.size();
	for (int i = 0; i < m; ++i) {
		vector<T> temp;
		for (int j = 0; j < n; ++j)
			temp.push_back(elements[j][i]);
		init.push_back(temp);
	}
	Matrix a(init);
	return a;
}

template<class T>
inline Matrix<T> Matrix<T>::findMinor(int r, int c)
{
	int m = elements.size();
	int n = elements[0].size();
	vector<vector<T>> vect;
	for (int i = 0; i < m; ++i) {
		vector<T> temp;
		for (int j = 0; j < n; ++j) {
			if (i!=r&&j != c)
				temp.push_back(elements[i][j]);
		}
		if(temp.size()!=0)
		vect.push_back(temp);
	}
	return Matrix<T>(vect);
}

template<class T>
Matrix<T> Matrix<T>::Gauss(Matrix<T> & right)
{
	if ((right.elements.size() != elements.size()) || (right.elements[0].size() != 1))
		throw exception("Can't solve!\n");

	int rows = elements.size();
	int columns = elements[0].size();
	int count;
	for (int i = 0; i < rows; ++i) {
		T a = elements[i][i];
		for (int m = 0; m < columns; m++) {
			elements[i][m] = elements[i][m]/a;
			
		}
		right.elements[i][0] = right.elements[i][0]/a;
		for (int n = 0; n < rows; ++n) {
			if (n != i) {
				count = 0;
				T b = elements[n][i];
				right.elements[n][0] = right.elements[n][0]-(right.elements[i][0] * b);
				for (int j = 0; j < columns; ++j) {
					elements[n][j] = elements[n][j]-(elements[i][j] * b);
					T z = 0;
					if (elements[n][j] == z)
						count++;
				}
				
				if (count == columns)
					throw exception("Determinant is 0. There's no solution.\n");
			}
		}
	}
	return right;
}

template<class T>
inline vector<T> Matrix<T>::getColumn(int i)
{
	vector<T> res;
	for (int j = 0; j < elements.size(); ++j) {
		T el = elements[j][i];
		res.push_back(el);
	}
	return res;
}

template<class T>
inline vector<T> Matrix<T>::ek(int len, int k)
{
	vector<T>e;
	for (int i = 0; i < len; ++i) {
		if (i == k)
			e.push_back(1);
		else
			e.push_back(0);
	}
	return e;
}

template<class T>
inline T Matrix<T>::norm(vector<T> x)
{
	T qrt = 0;
	for (int i = 0; i < x.size(); ++i) {
		qrt = qrt + x[i] * x[i];
	}
	T sqrt = pow(qrt, 0.5);
	return sqrt;
}

template<class T>
inline vector<T> Matrix<T>::alphae(T sqrt, vector<T> e)
{
	vector<T> res;
	for (int i = 0; i < e.size(); ++i) {
		res.push_back(sqrt*e[i]);
	}
	return res;
}

template<class T>
inline vector<T> Matrix<T>::findU(vector<T> x, int k)
{//u=x-aek
	vector<T> e = ek(x.size(), k);
	T norma = norm(x);
	vector<T> ae = alphae(norma, e);
	vector<T> u;
	int s = x.size();
	for (int i = 0; i < s; ++i)
		u.push_back(x[i] - ae[i]);
	return u;
}

template<class T>
inline vector<T> Matrix<T>::findV(vector<T> u)
{
	T modU = norm(u);
	vector<T> v;
	for (int i = 0; i < u.size(); ++i)
		v.push_back(u[i] / modU);
	return v;
}

template<class T>
inline Matrix<T> Matrix<T>::makeQRI(int size)
{
	vector<vector<T>> vect;
	for (int i = 0; i < size; ++i) {
		vector<T> temp;
		for (int j = 0; j < size; ++j) {
			if (i == j)
				temp.push_back(1);
			else
				temp.push_back(0);
		}
		vect.push_back(temp);
	}
	return Matrix<T>(vect);
}

template<class T>
Matrix<T> Matrix<T>::findQi()
{
	vector<T> x = getColumn(0);
	vector<T> u = findU(x, 0);
	vector<T> v = findV(u);
	int isize = v.size();
	Matrix<T> I = makeQRI(isize);
	vector<vector<T>> temp;
	temp.push_back(v);
	Matrix A(temp), B(temp);
	B = B.Transpose();
	T two = 2;
	B = B*2;
	B = B*A;
	Matrix<T> Q = I - B;
	return Q;
}

template<class T>
inline Matrix<T> Matrix<T>::makeBigger(int num)
{
	int m = elements.size();
	int n = m;
	vector<vector<T>>vect;

	for (int i = 0; i < m + num; ++i) {
		vector<T>temp;
		for (int j = 0; j < n + num; ++j) {
			if (i == j && i < num)
				temp.push_back(1);
			else if (i<num && j >= num)
				temp.push_back(0);
			else if (i >= num && j < num)
				temp.push_back(0);
			else
				temp.push_back(elements[i-num][j-num]);
		}
		vect.push_back(temp);
	}
	return Matrix<T>(vect);
}

template<class T>
Matrix<T> Matrix<T>::QRDec(Matrix & A)
{
	//A=QR, Q - ортогональна, R - верхній трикутник
	//метод хаусхолдера
	if (A.elements.size() != A.elements[0].size())
		throw exception("QR-decomposition is only possible for a square matrix!\n");

	int n = elements.size();
	vector <Matrix<T> > Qs, As, QAs;
	As.push_back(A);
	QAs.push_back(A);
	for (int i = 0; i < n - 1; ++i) {
		Qs.push_back(As[i].findQi());
		int diff = As[0].elements.size() - Qs[i].elements.size();
		if (diff != 0)
			Qs[i] = Qs[i].makeBigger(diff);
		QAs.push_back(Qs[i] * QAs[i]);
		As.push_back(QAs[i + 1].findMinor(0, 0));		
	}

	int q = Qs.size();
	for (int i = 0; i < q; ++i)
		Qs[i] = Qs[i].Transpose();

	Matrix<T>Q = Qs[0];
	for (int i = 1; i < q; ++i)
		Q = Q*Qs[i];

	Matrix<T> QT = Q.Transpose();
	Matrix<T> R = QT*A;
	A = R;
	return Q;
}

template<class T>
inline T Matrix<T>::findGreatestND(int & r, int & c)
{
	T n = elements.size();
	T max = 0, maxa=0;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			if (i != j) {
				if (abs(elements[i][j]) > maxa) {
					max = elements[i][j];
					maxa = abs(elements[i][j]);
					r = i;
					c = j;
				}
			}
		}
	}
	return max;
}

template<class T>
inline Matrix<T> Matrix<T>::makeGivens(int x, int y, T a, int n)
{
	vector <vector<T>>g;
	for (int i = 0; i < n; ++i) {
		vector<T>t;
		for (int j = 0; j < n; ++j) {
			if (i == j && i != x && i != y && j != x && j != y)
				t.push_back(1);
			else if (i == j && (i == x || j == y))
				t.push_back(cos(a));
			else if (i == y && j == x)
				t.push_back(sin(a));
			else if (i == x && j == y)
				t.push_back(-sin(a));
			else
				t.push_back(0);
		}
		g.push_back(t);
	}
	return Matrix<T>(g);
}

template<class T>
inline vector<T> Matrix<T>::Jacobi()
{
	Matrix<T> t = *this;
	t.Transpose();
	if (!(*this == t))
		throw exception("\nJacobi is only used for matrix that are symmetric.\n");
	Matrix<T> A = *this;
	int n = elements.size();
	for (int k = 0; k <50; ++k) {
		int j = 0, i = 0;
		T max = A.findGreatestND(i, j);
		T tan2x;
		if (A.elements[j][j] - A.elements[i][i] == 0)
			tan2x = M_PI_4;
		else
			tan2x = (2*A.elements[i][j]) / (A.elements[j][j] - A.elements[i][i]);
		T x = (atan(tan2x)) / 2;
		T c = cos(x);
		T s = sin(x);
		Matrix<T> AA = makeQRI(n);
		AA.elements[i][i] = c*c*A.elements[i][i] - 2 * s*c*A.elements[i][j] + s*s*A.elements[j][j];
		AA.elements[j][j] = s*s*A.elements[i][i] + 2 * s*c*A.elements[i][j] + c*c*A.elements[j][j];
		AA.elements[i][j] = (c*c - s*s)*A.elements[i][j] + s*c*(A.elements[i][i] - A.elements[j][j]);
		for (int k = 0; k < n; ++k) {
			if (k != i&&k != j) {
				AA.elements[i][k] = c*A.elements[i][k] - s*A.elements[j][k];
				AA.elements[k][i] = AA.elements[i][k];
			}
		}
		for (int k = 0; k < n; ++k) {
			if (k != i&&k != j) {
				AA.elements[j][k] = s*A.elements[i][k] + c*A.elements[j][k];
				AA.elements[k][i] = AA.elements[i][k];
			}
		}
		for(int k=0; k < n; ++k)
			for (int l = 0; l < n; ++l) {
				if (k != i&&k != j&&l != i&&l != j)
					AA.elements[k][l] = A.elements[k][l];
			}
		A = AA;
		T count = 0;
		for(int k1=0; k1<n; k1++)
			for (int k2 = 0; k2 < n; k2++) {
				if (k1 != k2)
						count=count + A.elements[k1][k2];
			}
		cout << A << endl;
		if (count < 0.00000001)
			break;
	}

	vector<T>res;
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < n; ++j)
			if (i == j)
				res.push_back(A.elements[i][i]);
	return res;
}


