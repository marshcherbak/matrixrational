#include <iostream>
#include <iomanip>
#include <vector>
#include "matrix.h"
#include "rational.h"
using namespace std;

int main() {
	Matrix<Rational> A, B, G,T1;

	try {
		cout << "Matrix A (for A+B, A-B, A*B, A^-1):\n";
		cin >> A;
		cout << "A:\n" << A;
		cout << "Matrix B (for A+B, A-B, A*B, Bx=G):\n";
		cin >> B;
		cout << "B:\n" << B;
		if ((A*A - B*B) == (A - B)*(A + B))
			cout << "\nYAAAY\n";
		Matrix<Rational> C = A + B;
		Matrix<Rational> D = A - B;
		cout << "A+B:\n" << C << endl;
		cout << "A-B:\n" << D << endl;

	}
	catch (exception c) {
		cout << c.what() << endl;
	}
	try {
		Matrix<Rational> E = A*B;
		cout << "A*B:\n" << E << endl;
	}
	catch (exception c) {
		cout << c.what() << endl;
	}

	try {
		Matrix<Rational> X = A;
		Matrix<Rational> ob = ~A;
		cout << "A^-1: \n" << ob << endl;
		Matrix<Rational> S = X*ob;
		cout << S <<endl;
		cout << "B:\n" << B;	
		cout << "Matrix G Nx1:\n";
	    cin >> G;	
		G = B.Gauss(G);
		cout << "X: \n" << G << endl;

	}
	catch (exception e) {
		cout << e.what() << endl;
	}

	
	try {

		vector<vector<double>> init = { {12,-51,4}, {6,167,-68}, {-4,24,-41} };
		Matrix<double> M(init), K(init);
		Matrix<double> Q = M.QRDec(M);
		cout << fixed;
		cout << "A=\n" << setprecision(2) << K << "Q=\n" << setprecision(5) << Q << "R=\n" << setprecision(2) << M;
		cout << "\nCheck: A=Q*R\n" << setprecision(2) << Q*M;

		//vector<vector<double>> t = { {4,-30,60,-35},{-30,300,-675,420},{60,-675,1620,-1050},{-35,420,-1050,700} };
		vector <vector<double>> t = { {2,0,0},{0,3,4},{0,4,9} };
		Matrix<double> J(t);
		cout << "\nJ matrix for Jacobi method:\n" << J;
		vector<double> r = J.Jacobi();
		cout << "Eigenvalues J:\n";
		for (auto rs : r) {
			cout << rs << endl;
		}
	}
	catch (exception e) {
		cout << e.what() << endl;
	}
	system("pause");
	return 0;
}