#include "rational.h"
#include <cmath>
#include <string>
using namespace std;
 // m/n
Rational::Rational():m(1),n(1){ }

Rational::Rational(double val){
	if (val - floor(val) == 0) {
		m = (int)val;
		n = 1;
	}
	else {
		string a = to_string(val);
		int k = a.size() - 1;
		while (k > 0) {
			if (a[k] != '0')
				break;
			a.erase(k, 1);
			k--;
		}
		string full;
		int i = 0;
		bool s = 0;
		if (a[i] == '-') {
			i++;
			s = 1;
		}
		for (; i < a.size(); ++i) {
			if (a[i] == '.' || a[i] == ',')
				break;
			full += a[i];
		}
		std::string::size_type sz;
		long f = stol(full, &sz);
		string nom;
		for (i = i + 1; i < a.size(); ++i) {
			nom += a[i];
		}
		m = stol(nom, &sz);

		n = 1;
		for (int j = 0; j < nom.size(); ++j)
			n *= 10;
		if (s)
			m = -m;
		m += n*f;
		simplify();
	}
}

Rational& Rational::operator=(const Rational & other)
{
	n = other.n;
	m = other.m;
	return *this;
}

bool Rational::operator==(Rational  other)
{
	return (m == other.m && n == other.n);
}


Rational Rational::operator+(const Rational & other)
{
	Rational res;
	int rm = m*other.n + other.m*n;
	int rn = n*other.n;
	res.m = rm;
	res.n = rn;
	res.simplify();
	return res;
}

Rational Rational::operator=(double el)
{
	Rational a(el);
	*this = a;
	return (*this);
}

Rational Rational::operator-(const Rational & other)
{
	Rational res;
	if (m == 0) {
		res.m = -other.m;
		res.n = other.n;
	}
	else {
		int rm = m*other.n - other.m*n;
		int rn = n*other.n;
		res.m = rm;
		res.n = rn;
		res.simplify();
	}
	
	return res;
}

Rational Rational::operator*(const Rational & other)
{
	Rational res;
	res.m = m*other.m;
	res.n = n*other.n;
	res.simplify();
	return res;
}

Rational Rational::operator/(const Rational & other)
{
	Rational ud;
	ud.m = other.n;
	ud.n = other.m;
	if (other.m < 0) {
		ud.m = -ud.m;
		ud.n = -ud.n;
	}
	return (*this)*ud;
}

Rational::operator double()
{
	double a = double(m);
	double b = double(n);
	return a / b;
}

int Rational::gcd(int a, int b)
{
	if (b == 0) return a;
	return gcd(b, a%b);
}

Rational& Rational::simplify()
{
	int k = gcd(abs(m), n);
	if (k != 1 && k != 0) {
		m /= k;
		n /= k;
	}
	if (((n > 10000 || m > 10000) && (abs(m) - n) < 100) || (n > 20000 && m > 20000)) {
		m = m / 2;
		n = n / 2;
		simplify();
	}
	return *this;
}

Rational Rational::approximate()
{
	m = m / 2;
	n = n / 2;
	simplify();
	return *this;
}

Rational Rational::pow(const Rational&r,double p)
{
	double a = r.m;
	double b = r.n;
	a = std::pow(a, p);
	b = std::pow(b, p);
	Rational res;
	res.m = a;
	res.n = b;
	res = res.simplify();
	return res;
}

ostream & operator<<(ostream & os, Rational & r)
{
	os << r.m;
	if(r.n!=1)
		cout << "/" << r.n << " ";
	return os;
}

istream & operator>>(istream & is, Rational & r)
{
	double a;
	is >> a;
	r = Rational(a);
	return is;
}
